
import java.util.ArrayList;

public class Main {

    private static String nameOfProgram;

    public static void main(String[] args) {
        ArrayList<Pet> myPetList = new ArrayList<Pet>();

        nameOfProgram = "Pet Tracker";

        Pet myPet = new Pet();
        System.out.println(myPet.numPets);
        myPet.setNumLegs(4);
        myPet.setName("Mycroft");

        myPet.feed();

        System.out.println(myPet.getName());
        System.out.println(myPet.getNumLegs());

        //-------------------------------------------

        Dragon myDragon = new Dragon("Rocco");
        //myDragon.setNumLegs(2);
        //myDragon.setName("Rocco");

        myDragon.feed("Goats");
        myDragon.feed();
        myDragon.breatheFire();

        System.out.println(myDragon.getName());
        System.out.println(myDragon.getNumLegs());

        //-------------------------------------------

        Pet secondDragon = new Dragon();
        secondDragon.setNumLegs(1);
        secondDragon.setName("Bear");

        secondDragon.feed();
        //secondDragon.breatheFire();

        System.out.println(secondDragon.getName());
        System.out.println(secondDragon.getNumLegs());
    }
}

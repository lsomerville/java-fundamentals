public class Dragon extends Pet {

    public Dragon(){
        this.setNumLegs(4);
    }
    public Dragon(String name){
        this();
        this.setName(name);
    }
    //method overloading
    public void feed() {
        System.out.println("Feed dragon some locusts");
    }
    public void feed(String food){
        System.out.println("Feed dragon some "+food);
    }
    public void breatheFire(){
        System.out.println("FIREEEEEEEE!!!!");
    }

    @Override
    public String toString(){
        return "This is a dragon with "+this.getNumLegs()+" legs and called "+this.getName();
    }
}

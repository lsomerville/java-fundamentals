public class StringsMain {
    public static void main(String[] args) {

        String bigString = "start: ";
        StringBuilder stringBuilder = new StringBuilder(bigString);

        for(int loop=0; loop<10; ++loop){
            //bigString += loop;
            stringBuilder.append(loop); //more memory efficient
        }
        System.out.println(stringBuilder);

        String testString = "Hello" + " " + "world" + " " + "after" + " " + "lunch";
        System.out.println(testString);

        Dragon stringyDragon = new Dragon("Smaug");
        System.out.println("Dragon created: " +stringyDragon.toString());
    }
}
